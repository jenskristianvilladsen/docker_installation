#!/bin/sh
# docker run script

#    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
#    see https://art-decor.org/mediawiki/index.php?title=Copyright
#    
#    Author: Melle Sieswerda
#
#    This program is free software; you can redistribute it and/or modify it under the terms of the
#    GNU Lesser General Public License as published by the Free Software Foundation; either version
#    2.1 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#    See the GNU Lesser General Public License for more details.
#
#    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

docker run -p 8080:8080 -p 8877:8877 -d -it --cap-add=SYS_PTRACE --security-opt=apparmor:unconfined --name art-decor art-decor

# Or, if you've already created the container:
# docker start art-decor
