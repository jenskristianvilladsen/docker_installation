#!/bin/bash
# automated installer for ART-DECOR: tomcat/orbeon, eXist-db, and eXist-db users

#    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
#    see https://art-decor.org/mediawiki/index.php?title=Copyright
#    
#    Author: Melle Sieswerda, Maarten Ligtvoet
#
#    This program is free software; you can redistribute it and/or modify it under the terms of the
#    GNU Lesser General Public License as published by the Free Software Foundation; either version
#    2.1 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#    See the GNU Lesser General Public License for more details.
#
#    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

# version
# 2018 08 09: initial version
# 2018 08 28: parametrized location_application
# 2018 09 05: parametrized exist and tomcat memory settings

echo The script $0 was called as $0 $@

echo '*************************************************************'
echo '*            Welcome to the ART-DECOR installer!            *'
echo '*************************************************************'
echo 'This script intends to make the installation a little easier,'
echo 'although several manual steps are still included.'
echo 'This script needs to run as root and will store any files in'
echo '/root'

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")

export TOMCAT_HOME=/var/lib/tomcat7
export EXIST_HOST=parasite
export ASSETS=/root/assets
# where is tomcat installed
location_application=/usr/share/tomcat7
# set default
tomcat_maxmem=1024

function tomcat_installation () {
    # Tomcat installation
    # Install & configure tomcat7, set JAVA_HOME to the Oracle version.
    echo 'Configuring tomcat7.'
    sed -i '/#JAVA_HOME=/a JAVA_HOME=/usr/lib/jvm/java-8-oracle/jre' /etc/default/tomcat7
    patch /etc/tomcat7/server.xml $ASSETS/server.xml.patch

    echo 'Creating directories for art-decor logs'
    mkdir ${location_application}/logs
    touch ${location_application}/logs/art-decor.log
    chown tomcat7 ${location_application}/logs/art-decor.log
    chmod 644 ${location_application}/logs/art-decor.log
    ln -s ${location_application}/logs/art-decor.log /var/log/tomcat7/

    echo 'Moving art-decor.war into place'
    mv $ASSETS/art-decor.war $TOMCAT_HOME/webapps
    # end of function tomcat_installation
}

function tomcat_memory_config ()
{
    # create tomcat setenv config file for memory settings
    touch ${location_application}/bin/setenv.sh
    tee -a "${location_application}/bin/setenv.sh" > /dev/null <<_EOF_
#!/bin/sh
#
# ENVARS for Tomcat environment
#
# JRE_HOME=/usr/java/jdk1.8.0_92/jre

CATALINA_OPTS="-Xmx${tomcat_maxmem}M -Xms${tomcat_maxmem}M"
# EOF
_EOF_

chmod u+x ${location_application}/bin/setenv.sh
# chown tomcat:root ${location_application}/bin/setenv.sh
}
# end of function tomcat_memory_config

install_exist_2 () {
    # eXist-db installation for eXist-db version 2.x
    # Unfortunately eXist-db requires user input during installation. Fortunately
    # there's a python script (by Melle) to take care of that :-)
    echo 'Running eXist-db installer'
    # argument 1 passed to this script is the eXist-db admin password
    # argument 2 passed to this script is the eXist-db Maximum memory in mb
    # example value: exist_maxmem=8192
    # argument 3 passed to this script is the eXist-db Cache memory in mb
    # example value: exist_cachemem=1024
    # argument 4 passed to this script is the eXist-db exist_installer_package
    python $ASSETS/install_existdb.py ${adminpassword} ${exist_maxmem} ${exist_cachemem} ${exist_installer_package}

    # Create symlinks
    ln -s /usr/local/exist_atp_2.2 /usr/local/exist_atp
    ln -s /usr/local/exist_atp/tools/wrapper/logs/ /var/log/exist_wrapper
    ln -s /usr/local/exist_atp/webapp/WEB-INF/logs /var/log/exist
    ln -s /usr/local/exist_atp/tools/wrapper/bin/exist.sh /etc/init.d/exist

    # Create a user, chown the installation and make sure the service is started
    # by with the correct uid
    adduser --system --group existdb
    chown -R existdb:existdb /usr/local/exist_atp*
    sed -i '/#RUN_AS_USER=/a RUN_AS_USER=existdb' /usr/local/exist_atp/tools/wrapper/bin/exist.sh

    # This fails when started as root!
    sudo -u existdb service exist start
    # end of function install_exist_2
}

install_exist_4 () {
    echo install_exist_4
    # eXist-db installation for eXist-db version 4.x
    $ASSETS/install_exist.sh --exist_file ${exist_installer_package} --password ${adminpassword} --exist_maxmem ${exist_maxmem} --exist_cachemem ${exist_cachemem} --exist_version 4
    # end of function install_exist_4
    
    # set timeouts in wrapper.conf larger
    tee -a "/usr/local/exist_atp/tools/yajsw/conf/wrapper.conf" > /dev/null <<_EOF_
    wrapper.startup.timeout = 6000
wrapper.shutdown.timeout = 1200
wrapper.ping.timeout = 900
_EOF_

#TODO
tail /usr/local/exist_atp/tools/yajsw/conf/wrapper.conf

    # start eXist-db
    # for centos 7: systemctl start eXist-db
    echo service eXist-db start
    service eXist-db start
    # TODO
    apt install net-tools
    netstat -anp
    echo sleep 30
    sleep 30
    echo service eXist-db status
    service eXist-db status
    echo TODO ISRUNNING install_exist_4
    curl localhost:8877/rest/db/system/install/

}

function exist_upload_config () {
    echo "Uploading configuration.xml with nictiz repository to eXist-db"
    curl -u admin:${adminpassword} --upload-file $ASSETS/configuration.xml http://localhost:8877/rest/db/apps/dashboard/
    # end of function exist_upload_config
}

function exist_packages_install () {
    echo "Uploading xquery script to install packages via http GET"
    curl -u admin:${adminpassword} --upload-file $ASSETS/install_exist_pkg.xquery http://localhost:8877/rest/db/system/install/

    for i in ${ASSETS}/exist-packages/*
    do
        echo "Installing eXist-db package '$i'"
        /usr/local/exist_atp/bin/client.sh -u admin -P ${adminpassword} -m /db/system/repo/ -p $i -ouri=xmldb:exist://127.0.0.1:8877/xmlrpc
        curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/install_exist_pkg.xquery?pkg=${i##*/}
    done

    # wait some time to finish package install
    echo ${SCRIPT}: sleep 30
    sleep 30
            echo ISRUNNING exist_packages_install
        curl localhost:8877/rest/db/system/install/

    # end of function exist_packages_install
}

function exist_create_users () {
    # Create all users that exist in assets/exist-users as .xml files
    for file in ${ASSETS}/exist-users/*.xml
    do
        echo "Creating eXist-db user from file: '${file}'"
        # only if placeholder exists, else use password present in the xml file
        if grep -q willbereplacedbyscript ${file}; then
            echo Placeholder exists in xml file
            # if password file exists
            passwordfile=${file}.password
            if [ -f ${passwordfile} ]; then
               echo Using password file
               . ${passwordfile}
            else
               newpwd=`tr -cd '[:alnum:]' < /dev/urandom | fold -w10 | head -n1`
               echo INFO: could not find password file. Generated password is: ${newpwd} for user ${file//.xml/}
            fi
            # replace placeholder willbereplacedbyscript by the password
            sed -i 's/willbereplacedbyscript/'${newpwd}'/g' ${file}
         else
            # placeholder does not exist
            # grab password from xml file, note that this might be not so secure
            echo WARN ${SCRIPT}: missing newpwd="willbereplacedbyscript" in file ${file} so using password from ${file}
            grep newpwd ${file}
         fi
         # set up user
         echo set up user
         curl -X POST -u admin:${adminpassword} -d @${file} -H "Content-Type: application/XML" http://localhost:8877/apps/art/modules/save-user.xquery
    done
    # end of function exist_create_users
}

function exist_authors () {
     echo Not implemented yet: exist_authors
    # TODO: add authors to a project
    # projects=demo5-
    # authoridcounter=100
    # add users to a project
    # for projectprefix in ${projects}
    # do
        # echo get project data for project ${projectprefix}
        # projecttemp=`curl -u admin:${adminpassword} http://localhost:8877/apps/art/modules/get-decor-project.xq?project=${projectprefix}`
        # echo OUTPUT ${projecttemp}
        # curl -X POST -u admin:${adminpassword} -H "Content-Type: application/XML" http://localhost:8877/apps/art/modules/save-decor-project.xquery?project=${projectprefix}
        
        # projecttemp=`curl -s -u admin:${adminpassword} http://localhost:8877/apps/art/modules/get-decor-project.xq?project=${projectprefix}`
        # projecttemp=`echo $projecttemp|sed 's/<reference/\n<author id="'${authoridcounter}'" username="'${author}'" email="" notifier="">'${author}'<\/author>\n<reference/'|sed 's/"/\\\"/g'`
        # curl -X POST -u admin:${adminpassword} --data "${projecttemp}" -H "Content-Type: application/XML" http://localhost:8877/apps/art/modules/save-decor-project.xquery?project=${projectprefix}
        # authoridcounter=$((authoridcounter+1))
    # done
# end of function exist_authors
}

function post_install () {
    # post-install hacks for eXist-db 4
    if [[ ${exist_installer_package} == *setup-4* ]]; then 
        echo post-install hacks for eXist-db 4
        # echo sleep 30
        # sleep 30
        echo ISRUNNING post_install
        curl localhost:8877/rest/db/system/install/
        service eXist-db start
        echo service eXist-db status
        service eXist-db status
        exist_upload_config
        exist_packages_install
        # different method via repo/
            echo curl -u admin:${adminpassword} --upload-file $ASSETS/ant/install_packages_from_repo/xquery_install_from_repository.xql http://localhost:8877/rest/db/system/install/
            curl -u admin:${adminpassword} --upload-file $ASSETS/ant/install_packages_from_repo/xquery_install_from_repository.xql http://localhost:8877/rest/db/system/install/
            echo    curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql
            # ART-1.9.57.xar DECOR-core-1.9.30.xar DECOR-services-1.9.21.xar ART-DECOR-system-services-1.9.5.xar terminology-1.9.29.xar DECOR-examples-1.9.0.xar xis-1.9.16.xar
            package=http://art-decor.org/ns/art
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/decor/core
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/decor/services
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/art-decor/system-services
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/terminology
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/examples
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/xis
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
        exist_create_users
    fi
# end of function post_install
}

function packages_install_from_repo () {
echo TODO packages_install_from_repo
            echo curl -u admin:${adminpassword} --upload-file $ASSETS/ant/install_packages_from_repo/xquery_install_from_repository.xql http://localhost:8877/rest/db/system/install/
            curl -u admin:${adminpassword} --upload-file $ASSETS/ant/install_packages_from_repo/xquery_install_from_repository.xql http://localhost:8877/rest/db/system/install/
 
            # ART-1.9.57.xar DECOR-core-1.9.30.xar DECOR-services-1.9.21.xar ART-DECOR-system-services-1.9.5.xar terminology-1.9.29.xar DECOR-examples-1.9.0.xar xis-1.9.16.xar
            package=http://art-decor.org/ns/art
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/decor/core
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/decor/services
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/art-decor/system-services
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/terminology
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/examples
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
            package=http://art-decor.org/ns/xis
            curl -u admin:${adminpassword} http://localhost:8877/rest/db/system/install/xquery_install_from_repository.xql?package=${package}
}

function usage ()
{
        echo "Usage: $0 --adminpassword <password> --exist_maxmem <1024> --exist_cachemem <128>"
        echo "
        -p | --adminpassword)   [required] provide an admin password for eXist-db
        -m | --exist_maxmem )   [required] the maxmemory for eXist-db
        -c | --exist_cachemem ) [required] the cachemem for eXist-db
        -t | --tomcat_maxmem )  [optional] the maxmemory for tomcat. Otherwise use default
        -h | --help )           Display this usage function
        "
        exit 1
# end of function usage
}

# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -pi | --post_install )      post_install=--post_install
                                    ;;
        -p | --adminpassword )      shift
                                    adminpassword=$1
                                    ;;
        -m | --exist_maxmem )       shift
                                    exist_maxmem=$1
                                ;;
        -c | --exist_cachemem )     shift
                                    exist_cachemem=$1
                                    ;;
        -t | --tomcat_maxmem )      shift
                                    tomcat_maxmem=$1
                                    ;;
        -eip | --exist_installer_package )      shift
                                    exist_installer_package=$1
                                    ;;
        -h | --help )               usage
                                    ;;
        * )                         usage
    esac
    shift
  done

cd /root

if [[ ${post_install} ]]; then
   # just run post_install
   post_install
   # exit after post_install to not run installer again
   exit
fi

tomcat_installation
# Configure the amount of memory for Tomcat
tomcat_memory_config
# tomcat installation done

if [[ ${exist_installer_package} == *setup-2* ]]; then 
  echo Installing a version 2.x eXist
  install_exist_2
elif [[ ${exist_installer_package} == *setup-3* ]]; then 
  echo Installing a version 3.x eXist with version 4 installer
  install_exist_4
elif [[ ${exist_installer_package} == *setup-4* ]]; then 
  echo Installing a version 4.x eXist with version 4 installer
  install_exist_4
   # packages_install_from_repo
  # exit
else
  ${SCRIPT}: WARNING: could not detect eXist-db version. Using default version 4 installer which might fail
  install_exist_4
fi

# TESTING TODO
echo service eXist-db status
service eXist-db status

exist_upload_config
exist_packages_install
if [[ ${exist_installer_package} == *setup-4* ]]; then 
  echo Installing a version 4.x eXist with version 4 installer: packages
  packages_install_from_repo
fi

exist_create_users
# exist_authors

        echo ISRUNNING before script end
        curl localhost:8877/rest/db/system/install/

echo ${SCRIPT}: completed