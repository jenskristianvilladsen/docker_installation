purpose: run an xquery
0. put your username and password in ../../backup_data_svn.password
REMOTE_EXIST_USERNAME=
REMOTE_EXIST_PASS=
Do not use quotes

1. put your xquery in xquery.xql
2. maybe edit the server settings in ant-run-xquery: default settings is localhost
3. run the xquery: ant -buildfile ./ant-run-xquery.xml all
