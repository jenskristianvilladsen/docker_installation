xquery version "3.0";

(: what is the decor file to write to :)

let $createIds :=
	let $allFiles   := collection('/db/apps/art-data/')
    let $file  := $allFiles//server-info
return
   ( 
   update value $file//url-art-decor-deeplinkprefix with 'draft' 
   )
   
return <done/>
