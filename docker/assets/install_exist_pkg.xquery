xquery version "3.0";

(:
    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
    see https://art-decor.org/mediawiki/index.php?title=Copyright
    
    Author: Melle Sieswerda, Maarten Ligtvoet

    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU Lesser General Public License as published by the Free Software Foundation; either version
    2.1 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.

    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html
:)

import module namespace repo="http://exist-db.org/xquery/repo";

(:  install xars from database :)
repo:install-and-deploy-from-db(concat("/db/system/repo/", request:get-parameter("pkg", "")))
 

 
