#!/usr/bin/env python
# eXist-db installer

#    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
#    see https://art-decor.org/mediawiki/index.php?title=Copyright
#    
#    Author: Melle Sieswerda, Maarten Ligtvoet
#
#    This program is free software; you can redistribute it and/or modify it under the terms of the
#    GNU Lesser General Public License as published by the Free Software Foundation; either version
#    2.1 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#    See the GNU Lesser General Public License for more details.
#
#    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import sys
import os
from subprocess import *
import time

# argument 1 passed to this script is the eXist-db admin password
adminpassword_py = sys.argv[1]
# for setting the password later on, we need a slightly different format
adminpassword_py_prompt ='Enter password:  [' + adminpassword_py + ']'
# argument 2 passed to this script is the eXist-db Maximum memory in mb
# example value: exist_maxmem=8192
exist_maxmem=sys.argv[2]
# argument 3 passed to this script is the eXist-db Cache memory in mb
# example value: exist_cachemem=1024
exist_cachemem = sys.argv[3]
# argument 4 passed to this script is the eXist-db exist_installer_package
# example value: exist_installer_package=eXist-db-setup-4.3.1_2018072610.jar
exist_installer_package = sys.argv[4]


print('-' * 80)
print('Welcome to the eXist-db installer!')
print('-' * 80)

def read_n_print(p, expected):
    """Careful: this could run indefinitely!"""
    while True:
        output = p.stdout.readline()
        print(output, end='')
        sys.stdout.flush()
    
        if output.strip() == expected:    
            break

def writeln(p, msg):
    print('>>>', msg)
    p.stdin.write(msg + '\n')    
    sys.stdout.flush()

dirname = os.path.dirname(sys.argv[0])
print("Changing directory to '{}'".format(dirname))
sys.stdout.flush()
os.chdir(dirname)

print("Starting installer ...")
installercommand = "/usr/bin/java -jar " + exist_installer_package
p = Popen(
    installercommand,
    stdin=PIPE, stdout=PIPE, stderr=PIPE,
    shell=True
)

'Enter password:  ['.format(adminpassword_py)
prompts = [
    ('Select target path [{}]'.format(dirname), '/usr/local/exist_atp_2.2'),
    ('press 1 to continue, 2 to quit, 3 to redisplay', '1'),
    ('Data dir:  [webapp/WEB-INF/data]', 'webapp/WEB-INF/data'),
    ('press 1 to continue, 2 to quit, 3 to redisplay', '1'),
    ('Enter password:  []', adminpassword_py),
    (adminpassword_py_prompt, ''),
    ('Maximum memory in mb: [1024]', exist_maxmem),
    ('Cache memory in mb: [128]', exist_cachemem),
    ('press 1 to continue, 2 to quit, 3 to redisplay', '1'),
]

for prompt in prompts:
    read_n_print(p, expected=prompt[0])
    writeln(p, prompt[1])

stdout, stderr = p.communicate()
print('-' * 80)
print(stdout)
print('-' * 80)
print(stderr)
