#!/bin/bash
# Docker install script for nginx in combination with lets encrypt, use production certificate in nginx
# Information, this script follows: https://www.humankode.com/ssl/how-to-set-up-free-ssl-certificates-from-lets-encrypt-using-docker-and-nginx
# Usage, see usage: ./<script> --help

#    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
#    see https://art-decor.org/mediawiki/index.php?title=Copyright
#    
#    Author: Maarten Ligtvoet, Carlo van Wyk
#
#    This program is free software; you can redistribute it and/or modify it under the terms of the
#    GNU Lesser General Public License as published by the Free Software Foundation; either version
#    2.1 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#    See the GNU Lesser General Public License for more details.
#
#    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

# version
# 2018 08 06: initial version
# 2018 08 07: add option for self-signed certificate

# set variables
# assumeyes option for automated installation, default is to ask at all times
ASSUME_YES=
# location for docker nginx
docker_nginx_dir_src=/docker/letsencrypt-docker-nginx/src/
# where is docker-compose binary located
docker_compose_binary="/usr/local/bin/docker-compose"
# for usage function
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

echo The script $0 was called as $0 $@

function variable_set ()
{
    # options
    #  --DONT_DISPLAY: do not display contents of variable if set, for passwords, etc

    # process arguments
    case $2 in
        --DONT_DISPLAY )          local DONT_DISPLAY=1
                                  ;;
    esac

    # check whether variable is set
    if [[ ! -v $1 ]];
        then
           echo $0 ERROR: $1 is not set!
           echo "Either put it in the file 'settings' or see --help"
           echo ""
           usage
        else
            if [[ ${DONT_DISPLAY} ]]
                then
                    echo "    ${1} = <not showing>"
                else
                    echo "    ${1} = ${!1}"
            fi
    fi
}
# end of function variable_set

function sanity_check ()
{
    # sanity check
    # set variables
    # get common parameters from settings script
    # note that for this script all variables are passed as argument or supplied above
    . ./settings

    # these parameters needs to be set:
    echo $0 Installing with the following settings:
    variable_set domain
}
# end of function sanity_check

function yum_prerequisites ()
{
    # install required software with yum
    yum install -y openssl
}
# end of function yum_prerequisites

function abort_installation ()
{
    echo Cancelling installation
    exit 2
}
# end of function abort_installation

function delete_dirs ()
{
    # stop old containers
    docker stop letsencrypt-nginx-container
    docker stop production-nginx-container
    docker rm -v production-nginx-container
    # remove old directories
    rm -Rf ${docker_nginx_dir_site}
}
# end of function delete_dirs

function mkdir_dirs ()
{
    # create directories
    mkdir -p ${docker_nginx_dir_site}/production-site
    mkdir -p ${docker_nginx_dir_site}/dh-param
}
# end of function mkdir_dirs

function docker_compose ()
{
    # create docker compose file
    cd ${docker_nginx_dir_site}
    
    # The docker-compose does the following:
    # Allows ports 80 and 443
    # Maps the production Nginx configuration file into the container
    # Maps the production site content into the container
    # Maps a 2048 bit Diffie Hellman key exchange file into the container
    # Maps the public and private keys into the container
    # Sets up a docker network

    touch ${docker_nginx_dir_site}docker-compose.yml
    tee -a "docker-compose.yml" > /dev/null <<_EOF_
version: '3.1'

services:

  production-nginx-container:
    container_name: 'production-nginx-container'
    image: nginx:latest
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - ./production_nginx.conf:/etc/nginx/conf.d/default.conf
      - ./production-site:/usr/share/nginx/html
      - ./dh-param/dhparam-2048.pem:/etc/ssl/certs/dhparam-2048.pem
      - /docker-volumes/etc/letsencrypt/live/${domain}/fullchain.pem:/etc/letsencrypt/live/${domain}/fullchain.pem
      - /docker-volumes/etc/letsencrypt/live/${domain}/privkey.pem:/etc/letsencrypt/live/${domain}/privkey.pem
    networks:
      - docker-network

networks:
  docker-network:
    driver: bridge
_EOF_
}
# end of function docker_compose

function nginx_config_file ()
{
    # create nginx file
    touch ${docker_nginx_dir}production_nginx.conf
    tee -a "production_nginx.conf" > /dev/null <<_EOF_
server {
    listen      80;
    listen [::]:80;
    server_name ${domain} www.${domain};

    location / {
        rewrite ^ https://$host$request_uri? permanent;
    }

    #for certbot challenges (renewal process)
    location ~ /.well-known/acme-challenge {
        allow all;
        root /data/letsencrypt;
    }
}

#https://${domain}
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name ${domain};

    server_tokens off;

    ssl_certificate /etc/letsencrypt/live/${domain}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${domain}/privkey.pem;

    ssl_buffer_size 8k;

    ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;

    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    ssl_ecdh_curve secp384r1;
    ssl_session_tickets off;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8;
    
    # fix for error: upstream timed out (110: Connection timed out
    # increase proxy timeout for the whole server
    proxy_read_timeout 300;

    root /usr/share/nginx/html;
    
        # rewrite testserver/art-decor to orbeon port 8080/art-decor
        location /art-decor {
                proxy_pass http://${domain}:8080/art-decor;
        }
        # end of location /art-decor

        # rewrite testserver/styles to port 8877/
        location /styles/ {
                proxy_pass http://${domain}:8877/styles/;
        }
        # end of location /styles

### art-decor packages

        # rewrite testserver/ada-data
        location /ada-data {
                proxy_pass http://${domain}:8877/ada-data;
        }
        # end of location /ada-data

        # rewrite testserver/ada
        location /ada {
                proxy_pass http://${domain}:8877/ada;
        }
        # end of location /ada

        # rewrite testserver/art to orbeon port 8080/art-decor
        location /art {
                proxy_pass http://${domain}:8877/art;
                # proxy_pass http://${domain}:8080/art-decor;
        }
        # end of location /art

        # rewrite testserver/decor to port 8877/
        location /decor/ {
                proxy_pass http://${domain}:8877/decor/;
        }
        # end of location /decor

        # rewrite testserver/fhir to port 8877/
        location /fhir/ {
                proxy_pass http://${domain}:8877/fhir/;
        }
        # end of location /fhir

        # rewrite testserver/decor/services to port 8877/
        location /decor/services {
                proxy_pass http://${domain}:8877/decor/services;
        }
        # end of location /decor/services

        # rewrite testserver/services to port 8877/
        location /services {
                proxy_pass http://${domain}:8877/decor/services;
        }
        # end of location /services

        # rewrite testserver/temple to port 8877/
        location /temple {
                proxy_pass http://${domain}:8877/apps/temple;
        }
        # end of location /temple

        
        # rewrite decor.nictiz.nl/terminology to port 8877/
        location /terminology {
                proxy_pass http://${domain}:8877/terminology;
        }
        # end of location /terminology

        
        # rewrite testserver/xis to port 8877/
        location /xis {
                proxy_pass http://${domain}:8877/xis;
        }
        # end of location /xis

        
### end of art-decor packages   
}

#https://www.${domain}
server {
    server_name www.${domain};
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_tokens off;

    ssl on;

    ssl_buffer_size 8k;
    ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;
    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    ssl_ecdh_curve secp384r1;
    ssl_session_tickets off;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8 8.8.4.4;

    ssl_certificate /etc/letsencrypt/live/${domain}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${domain}/privkey.pem;

    return 301 https://${domain}$request_uri;
}
_EOF_
}
# end of function nginx_config_file

function nginx_index_file ()
{
    # create nginx file
    touch ${docker_nginx_dir_site}production-site/index.html
    tee -a "production-site/index.html" > /dev/null <<_EOF_
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Let's Encrypt Production Cert Issue Site</title>
</head>
<body>
    <h1>Oh, hai there on ${domain}!</h1>
    <p>
        This is the temporary site that will only be used for the very first time SSL certificates are issued by Let's Encrypt's
        certbot.
    </p>
</body>
</html>
_EOF_
}
# end of function nginx_index_file

# for testing dh site = 128 instead of 2048

function generate_dh ()
{
    echo Generate a DH Param file only if not present
    dhparam_file=/docker/letsencrypt-docker-nginx/src/${domain}/dh-param/dhparam-2048.pem
    if [ ! -f "${dhparam_file}" ]; then
      openssl dhparam -out ${dhparam_file} 2048
    else
      echo Info: not generating diffie helman file since already present: ${dhparam_file}
    fi
}
# end of function generate_dh

function run_docker_nginx_container ()
{
    # Before running the Certbot command, 
    # spin up a Nginx container in Docker to ensure the temporary Nginx site is up and running
    cd ${docker_nginx_dir_site}
    ${docker_compose_binary} up -d
}
# end of function run_docker_nginx_container

function check_website_live ()
{
    echo Optional: Navigate to https://${domain} and check for yourself that the website displays the default 'O hai there' index page

    if [[ ${ASSUME_YES} ]]; then
       echo Continuing
    else
       read -p "Continue (y/n)? " choice
       case "$choice" in
         y|Y ) echo Continuing;;
         n|N ) abort_installation;;
         * ) abort_installation;;
       esac
    fi
}
# end of function check_website_live

function usage ()
{
    echo "Usage: Usage: ${SCRIPT} --domain MY.DOMAIN.COM
"
        echo "
        Options:
        -y | --yes             Automated installation: assume yes and continue with default options
        -d | --domain          Which domain to use with SSL certificate
        --selfsignedcert       If you do not use DNS: Do not use let's encypt to issue certificate, but use standalone openssl for self-signed certificate. 
                               Note that using a self-signed certificate is not secure!
        -h | --help )          Display this usage function
        "
    exit 1
}
# end of function usage

# main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -y | --yes )            ASSUME_YES=--assumeyes
                                ;;
        -d | --domain )         shift
                                domain=$1
                                ;;
        --selfsignedcert )      selfsignedcert=1
                                ;;
        -h | --help )           usage
                                ;;
        * )                     usage
    esac
    shift
  done

if [[ ${ASSUME_YES} ]]; then
   echo Continuing
else
   echo Starting the Docker installation for nginx and lets encrypt production run
   echo CAUTION: this script will remove previous config files for this container
   read -p "Continue (y/n)? " choice
   case "$choice" in
     y|Y ) echo Continuing;;
     n|N ) abort_installation;;
     * ) abort_installation;;
   esac
fi

# run sanity check
sanity_check

# create a directory config from the domain argument
docker_nginx_dir_site=${docker_nginx_dir_src}/${domain}/

yum_prerequisites
delete_dirs
mkdir_dirs
docker_compose
nginx_config_file
nginx_index_file
generate_dh
if [[ ${selfsignedcert} ]]; then
    echo Skipping lets encrypt installation, use openssl selfsigned certificate
    cd ${SCRIPTPATH}
    # use option to skip nginx config since we do that above in docker
    # --directory: where to put certificate
    # ${SCRIPT}: running: ./install_self_signed_certificate.sh --skipnginx --directory /docker-volumes/etc/letsencrypt/live/${domain}/
    ./install_self_signed_certificate.sh --skipnginx --directory /docker-volumes/etc/letsencrypt/live/${domain}/ --domain ${domain}
fi
run_docker_nginx_container
check_website_live

echo END of docker installation for nginx and lets encrypt production run for domain ${domain}
# EOF