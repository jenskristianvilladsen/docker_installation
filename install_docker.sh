#!/bin/bash
# simple docker install script
# Information, this script follows: https://docs.docker.com/engine/installation/linux/docker-ce/centos
# usage: run this script with sudo permissions: sudo ./<script>
# usage: run this script with sudo permissions and assume yes for yum: sudo ./<script> --yes
# to run vonk style: sudo ./<script> --yes
# to run ART-DECOR style: sudo ./<script> --yes --location /usr/local/docker/art-decor/ --p 8877

#    Copyright (C) ART-DECOR Expert Group and ART-DECOR Open Tools
#    see https://art-decor.org/mediawiki/index.php?title=Copyright
#    
#    Author: Maarten Ligtvoet
#
#    This program is free software; you can redistribute it and/or modify it under the terms of the
#    GNU Lesser General Public License as published by the Free Software Foundation; either version
#    2.1 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#    See the GNU Lesser General Public License for more details.
#
#    The full text of the license is available at http://www.gnu.org/copyleft/lesser.html

# version
# 2017 09 11: initial version
# 2017 10 23: add docker-compose
# 2017 10 24: assume yes for automated installs
# 2018 01 04: still need to delete by hand rm -Rf /usr/local/docker/vonk/
# 2018 07 23: generalized vonk to application

# set variables
# get common parameters from settings script
. ./settings
    # docker_version
    # vonk_port

# set variables
# info docker-compose installation
# https://docs.docker.com/compose/install/#master-builds
docker_compose_url=https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m`
# assumeyes when using yum, default is to ask at all times
ASSUME_YES=
# location where application is located in docker, override with -l
application_location=/usr/local/docker/vonk/
# port where application will be run, override with --port 
application_port=${vonk_port}

function check_variable_has_content ()
{
    # check whether variable has content
    # echo variable to check = $1
    if [ "$1" != "" ]; then
        # if variable has content return 1
        return 1
    else
        return 0
    fi
}
# end of function check_variable_has_content

function delete_application ()
{
    # remove old docker containers
    echo Removing docker installation
    echo Can give a warning: ./install_docker.sh: line ..: docker: command not found
    # containers should be stopped before removing
        docker stop `docker ps --no-trunc -aq`
        docker rm `docker ps --no-trunc -aq`
        # https://docs.docker.com/engine/reference/commandline/system_prune/#usage
        docker system prune --all --force
        docker system prune --all --force --volumes
        # still need to delete by hand
        rm -Rf ${application_location}
    # first remove old versions
        yum ${ASSUME_YES} remove docker docker-common docker-selinux docker-engine
}
# end of function delete_application

function check_port_available ()
{
    # info https://bash.cyberciti.biz/guide/The_exit_status_of_a_command
    echo "About to install docker on port: ${application_port}, checking if the port is free"
    netstat -anp|grep :${application_port}
    # store exit status of grep
    # if found grep will return 0 exit status
    # if not found, grep will return a nonzero exit status
    status=$?

    if test $status -eq 0
      then
        echo "ERROR: Port ${application_port} seems in use already."
        read -p "Continue anyway (y/n)? " choice
          case "$choice" in
          y|Y ) echo Continuing;;
          n|N ) abort_installation;;
          * ) abort_installation;;
        esac
    else
         echo "Port is free, continuing"
    fi
}
# end of function check_port_available

function yum_prerequisites ()
{
    # install required software with yum
    # Install required packages. yum-utils provides the yum-config-manager utility, and device-mapper-persistent-data and lvm2 are required by the devicemapper storage driver.
        yum install -y yum-utils device-mapper-persistent-data lvm2
}
# end of function yum_prerequisites

function abort_installation ()
{
    echo Cancelling installation
    exit 2
}
# end of function abort_installation

function install_docker_repo ()
{
    # set up the stable repository
    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
}
# end of function install_docker_repo

function update_yum ()
{
    echo
        echo You will be prompted to accept the GPG key
        echo Verify that the fingerprint is correct, and if so, accept the key.
        echo The fingerprint should match 060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35.
    # Update the yum package index.
    yum makecache fast
    # you will be prompted to accept the GPG key, and the key.s fingerprint will be shown.
}
# end of function update_yum

function docker_choose_version ()
{
    echo On production systems, you should install a specific version of Docker CE instead of always using the latest. List the available versions.
        yum list docker-ce.x86_64  --showduplicates | sort -r

        echo Which Docker version would you like to install?
        echo Check this website for version release notes: https://docs.docker.com/release-notes/docker-ce/
        echo Example = 17.06.2.ce-1.el7.centos
    read docker_version

    # save the version to settings for the next installation
        tee -a "settings" > /dev/null <<_EOF_
docker_version=${docker_version}
_EOF_
}
# end of function docker_choose_version

function install_docker ()
{
    # install docker
        yum ${ASSUME_YES} install docker-ce-${docker_version}
}
# end of function install_docker

function install_docker_compose ()
{
# info docker-compose installation
# https://docs.docker.com/compose/install/#master-builds
docker_compose_url=https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m`

curl -L ${docker_compose_url} -o /usr/local/bin/docker-compose
# set permissions
chmod +x /usr/local/bin/docker-compose

}
# end of function install_docker_compose

function start_docker ()
{
    # start docker
        systemctl start docker
}
# end of function start_docker

function docker_test ()
{
    # test docker
    docker run hello-world
}
# end of function docker_test

function post_installation ()
{
    # Linux postinstall
    # Info https://docs.docker.com/engine/installation/linux/linux-postinstall/

    # start docker after system startup
    systemctl enable docker
}
# end of function post_installation

# main logic
# process arguments
  while [ "$1" != "" ]; do
    case $1 in
        -y | --yes )            ASSUME_YES=--assumeyes
                                ;;
        -s | --skipdockertest )     skip_docker_test=--skip_docker_test
                                ;;
        -l | --location )       shift
                                application_location=$1
                                ;;
        --port )                shift
                                application_port=$1
                                ;;
    esac
    shift
  done
echo Starting the Docker installation

echo CAUTION: this script will delete any previous Docker Installation
if [[ ${ASSUME_YES} ]]; then
   delete_application
else
    read -p "Continue (y/n)? " choice
    case "$choice" in
      y|Y ) delete_application;;
      n|N ) abort_installation;;
      * ) abort_installation;;
    esac
fi

# TODO TURN ON check_port_available
yum_prerequisites
install_docker_repo
update_yum

# If docker_version is not set in settings, get it in function docker_choose_version and store to settings
# check if docker_version is set
check_variable_has_content ${docker_version} && docker_choose_version

if [[ ${ASSUME_YES} ]]; then
   echo Continuing
else
   echo About to install docker version: ${docker_version}
   read -p "Continue (y/n)? " choice
   case "$choice" in
     y|Y ) echo Continuing;;
     n|N ) abort_installation;;
     * ) abort_installation;;
   esac
fi

install_docker
install_docker_compose
start_docker
if [[ ${skip_docker_test} ]]; then
    echo Skipping docker test
else
       docker_test
fi
post_installation

echo END of docker installation

# EOF